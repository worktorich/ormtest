﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ORMTest.Core
{
    public interface IORM
    {
        void TestInsert(int count);

        List<object> GetList();

        List<object> Page(int pageSize, int pageNumber);

        object GetInfo(int id);
        object GetOne(int id);
        int Count();

        void DeleteOne(int id);

        void DeleteAll();

        void UpdateOne<T>(T t);

        void TruncateTable();
    }
}
