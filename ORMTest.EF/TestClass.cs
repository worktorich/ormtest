﻿using ORMTest.Core;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ORMTest.EF
{
    public class TestClass : IORM
    {
        public void TestInsert(int count)
        {
            using (ORMTestEntities db = new EF.ORMTestEntities())
            {
                for (int i = 0; i < count; i++)
                {
                    Orders o = new Orders { AddTime = DateTime.Now, OrderID = i, Osn = i + "s", PayAmount = i * 10 };
                    OrderProducts op1 = new OrderProducts { OrderProductID = i, OrderID = o.OrderID, CateID = i, ProductName = "test" + i };
                    OrderProducts op2 = new OrderProducts { OrderProductID = i + 100000000, OrderID = o.OrderID, CateID = i + count, ProductName = "test" + i + count };
                    db.Orders.Add(o);
                    db.OrderProducts.Add(op1);
                    db.OrderProducts.Add(op2);
                    db.SaveChanges();
                }
            }
        }

        public List<object> GetList()
        {
            using (ORMTestEntities db = new EF.ORMTestEntities())
            {
                return db.Orders.AsNoTracking().ToList<object>();
            }
        }

        public List<object> Page(int pageSize, int pageNumber)
        {
            using (ORMTestEntities db = new EF.ORMTestEntities())
            {
                return db.Orders.AsNoTracking().OrderBy(X => X.OrderID).Skip(pageSize * (pageNumber - 1)).Take(pageSize).ToList<object>();
            }
        }

        public object GetInfo(int id)
        {
            using (ORMTestEntities db = new EF.ORMTestEntities())
            {
                dynamic dynamicObject = new ExpandoObject();
                dynamicObject.Order = db.Orders.Find(id);
                dynamicObject.OrderProductList = db.OrderProducts.Where(x => x.OrderID == id);
                return dynamicObject;
            }
        }

        public int Count()
        {
            using (ORMTestEntities db = new EF.ORMTestEntities())
            {
                return db.Orders.Count();
            }
        }



        public object GetOne(int id)
        {
            using (ORMTestEntities db = new EF.ORMTestEntities())
            {
                return db.Orders.Find(id);
            }
        }


        public void DeleteOne(int id)
        {
            using (ORMTestEntities db = new EF.ORMTestEntities())
            {
                var order = db.Orders.Find(id);
                db.Orders.Remove(order);
                db.SaveChanges();
            }
        }

        public void DeleteAll()
        {
            using (ORMTestEntities db = new EF.ORMTestEntities())
            {
                db.Database.ExecuteSqlCommand("DELETE Orders");
            }
        }


        public void UpdateOne<T>(T t)
        {
            using (ORMTestEntities db = new EF.ORMTestEntities())
            {
                Orders o = t as Orders;
                var sqlt = db.Orders.Find(o.OrderID);
                sqlt.Osn = o.Osn;
                db.SaveChanges();
            }
        }

        public void TruncateTable()
        {
            using (ORMTestEntities db = new EF.ORMTestEntities())
            {
                db.Database.ExecuteSqlCommand("truncate table Orders");
                db.Database.ExecuteSqlCommand("truncate table orderproducts");

            }
        }


    }
}
