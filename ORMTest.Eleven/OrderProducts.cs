using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ORMTest.Eleven
{
	public class OrderProducts
	{

        public int OrderProductID { get; set; }

        public string ProductName { get; set; }

        public int OrderID { get; set; }

        public int CateID { get; set; }
	}
}
