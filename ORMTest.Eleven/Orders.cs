using Eleven;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ORMTest.Eleven
{
	public class Orders
	{
        [DbColumn(AutoIncrement = false, IsPrimaryKey = true)]
        public int OrderID { get; set; }

        public string Osn { get; set; }

        public DateTime AddTime { get; set; }

        public decimal PayAmount { get; set; }
	}
}
