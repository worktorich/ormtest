﻿using Eleven;
using ORMTest.Core;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ORMTest.Eleven
{
    public class TestClass : IORM
    {
        public static Database db = new Database("ConnectionStrings");

        public void TestInsert(int count)
        {

            for (int i = 0; i < count; i++)
            {
                Orders o = new Orders { AddTime = DateTime.Now, OrderID = i, Osn = i + "s", PayAmount = i * 10 };
                OrderProducts op1 = new OrderProducts { OrderProductID = i, OrderID = o.OrderID, CateID = i, ProductName = "test" + i };
                OrderProducts op2 = new OrderProducts { OrderProductID = i + count, OrderID = o.OrderID, CateID = i + count, ProductName = "test" + i + count };
                //DefaultConnectDB.GetInstance().Insert(o);
                //db.Insert(o);
                //Order.repo.Insert(o);
                db.Insert(o).Execute();
                db.Insert(op1).Execute();
                db.Insert(op2).Execute();
            }

        }

        public List<object> GetList()
        {
            SQL sql = new SQL("select * from orders");
            return db.ToList<Orders>(sql).ToList<object>();
        }

        public List<object> Page(int pageSize, int pageNumber)
        {
            return db.Query<Orders>().OrderBy(x => x.OrderID).Skip(pageSize * (pageNumber - 1)).Take(pageSize).ToList().ToList<object>();
        }

        public object GetInfo(int id)
        {
            dynamic dynamicObject = new ExpandoObject();
            dynamicObject.Order = db.Query<Orders>().Where(x => x.OrderID == id).ToSingle();
            dynamicObject.OrderProductList = db.Query<OrderProducts>().Where(x => x.OrderID == id);
            return dynamicObject;
        }

        public int Count()
        {
            return db.Query<Orders>().Count();
        }



        public object GetOne(int id)
        {
            return db.Query<Orders>().Where(x => x.OrderID == id).ToSingle();
        }


        public void DeleteOne(int id)
        {
            db.Delete<Orders>(x => x.OrderID == id).Execute();
        }

        public void DeleteAll()
        {
            db.Delete<Orders>(x => x.OrderID != -1).Execute();
        }


        public void UpdateOne<T>(T t)
        {
            Orders o = t as Orders;
            db.Update(o).Execute();
        }

        public void TruncateTable()
        {
            db.ExecuteNonQuery(new SQL("truncate table Orders;truncate table orderproducts"));
        }


    }
}
