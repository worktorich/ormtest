﻿/**  版本信息模板在安装目录下，可自行修改。
* Categories.cs
*
* 功 能： N/A
* 类 名： Categories
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2017/7/12 10:21:28   N/A    初版
*
* Copyright (c) 2012 Maticsoft Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
namespace ORMTest.Lin.Model.Model
{
	/// <summary>
	/// Categories:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class Categories
	{
		public Categories()
		{}
		#region Model
		private int _cateid;
		private string _catename;
		/// <summary>
		/// 
		/// </summary>
		public int CateID
		{
			set{ _cateid=value;}
			get{return _cateid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string CateName
		{
			set{ _catename=value;}
			get{return _catename;}
		}
		#endregion Model

	}
}

