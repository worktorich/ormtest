﻿
using OSM.Models;
using OSM.Models.Tools;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace ORMTest.Lin.Model.Model
{
    /// <summary>
    /// OrderProducts:实体类(属性说明自动提取数据库字段的描述信息)
    /// </summary>
    [Serializable]
    public partial class OrderProducts : EntityView
    {
        public OrderProducts()
        { }
        #region Model
        private int _orderproductid;
        private string _productname;
        private int? _orderid;
        private int? _cateid;
        /// <summary>
        /// 
        /// </summary>
        public int OrderProductID
        {
            set { _orderproductid = value; }
            get { return _orderproductid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string ProductName
        {
            set { _productname = value; }
            get { return _productname; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? OrderID
        {
            set { _orderid = value; }
            get { return _orderid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? CateID
        {
            set { _cateid = value; }
            get { return _cateid; }
        }
        #endregion Model


        public override string AutoID
        {
            get
            {
                if (OrderProductID < 0)
                    return string.Empty;
                return this.OrderProductID.ToString();
            }
        }

        protected override void ToEntity(IDataReader reader)
        {
            base.ToEntity(reader);
            this.OrderID = Convert.ToInt32(reader["OrderID"]);
            this.OrderProductID = Convert.ToInt32(reader["OrderProductID"]);
            this.CateID = Convert.ToInt32(reader["CateID"]);
            this.ProductName = reader["ProductName"].ToString();
        }

        static string InsertSql = "INSERT INTO [OrderProducts] ([OrderProductID] ,[ProductName] ,[OrderID] ,[CateID]) VALUES (@OrderProductID ,@ProductName ,@OrderID ,@CateID)";

        public static void Insert(OrderProducts orderProduct)
        {
            SqlParameter[] ps = {
                                    AddInParam("@CateID" , SqlDbType.VarChar, 50 , orderProduct.CateID  ) ,
                                    AddInParam("@OrderID" , SqlDbType.VarChar, 50 , orderProduct.OrderID  ) ,
                                    AddInParam("@OrderProductID" , SqlDbType.VarChar, 50  , orderProduct.OrderProductID ) ,
                                    AddInParam("@ProductName" ,  SqlDbType.VarChar, 50 , orderProduct.ProductName )
                                    };
            AccessDB.Execute(InsertSql, ps);
        }

        static string UpdateSql = "UPDATE [OrderProducts] SET [ProductName] = ProductName ,[OrderID] = OrderID ,[CateID] = CateID WHERE [OrderProductID] = @OrderProductID";

        public static void Update(OrderProducts orderProduct)
        {
            SqlParameter[] ps = {
                                    AddInParam("@CateID" , SqlDbType.VarChar, 50 , orderProduct.CateID  ) ,
                                    AddInParam("@OrderID" , SqlDbType.VarChar, 50 , orderProduct.OrderID  ) ,
                                    AddInParam("@OrderProductID" , SqlDbType.VarChar, 50  , orderProduct.OrderProductID ) ,
                                    AddInParam("@ProductName" ,  SqlDbType.VarChar, 50 , orderProduct.ProductName )
                                    };
            AccessDB.Execute(UpdateSql, ps);
        }
        static string GetListSql = "SELECT [OrderProductID] ,[ProductName],[OrderID],[CateID] FROM [OrderProducts] WHERE [orderid] = @orderid";


        public static List<OrderProducts> GetList(int orderID)
        {
            List<OrderProducts> list = new List<OrderProducts>();
            OrderProducts o;
            SqlParameter[] ps = {
                                     new SqlParameter("@OrderID" , orderID ),
                                    };
            AccessDB.SelectReader(GetListSql, ps, delegate (IDataReader reader)
            {
                while (reader.Read())
                {
                    o = new OrderProducts();
                    o.ToEntity(reader);
                    list.Add(o);
                }
            });
            return list;
        }


    }
}

