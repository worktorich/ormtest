﻿
using OSM.Models;
using OSM.Models.Tools;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace ORMTest.Lin.Model.Model
{
    /// <summary>
    /// Orders:实体类(属性说明自动提取数据库字段的描述信息)
    /// </summary>
    [Serializable]
    public partial class Orders : EntityView
    {
        public Orders()
        { }
        #region Model
        private int _orderid;
        private string _osn;
        private DateTime? _addtime;
        private decimal? _payamount;
        /// <summary>
        /// 
        /// </summary>
        public int OrderID
        {
            set { _orderid = value; }
            get { return _orderid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Osn
        {
            set { _osn = value; }
            get { return _osn; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? AddTime
        {
            set { _addtime = value; }
            get { return _addtime; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? PayAmount
        {
            set { _payamount = value; }
            get { return _payamount; }
        }
        #endregion Model

        public override string AutoID
        {
            get
            {
                if (OrderID < 0)
                    return string.Empty;
                return this.OrderID.ToString();
            }
        }

        protected override void ToEntity(IDataReader reader)
        {
            base.ToEntity(reader);
            this.OrderID = Convert.ToInt32(reader["OrderID"]);
            this.Osn = reader["Osn"].ToString();
            this.PayAmount = Convert.ToDecimal(reader["PayAmount"]);
            this.AddTime = Convert.ToDateTime(reader["AddTime"]);
        }

        static string InsertSql = "INSERT INTO [Orders] ([OrderID] ,[Osn] ,[AddTime] ,[PayAmount]) VALUES (@OrderID ,@Osn ,@AddTime ,@PayAmount)";

        public static void Insert(Orders order)
        {
            SqlParameter[] ps = {
                                    AddInParam("@AddTime" , SqlDbType.VarChar, 50 , order.AddTime ) ,
                                    AddInParam("@OrderID" , SqlDbType.VarChar, 50 , order.OrderID  ) ,
                                    AddInParam("@Osn" , SqlDbType.VarChar, 50  , order.Osn ) ,
                                    AddInParam("@PayAmount" ,  SqlDbType.VarChar, 50 , order.PayAmount )
                                    };
            AccessDB.Execute(InsertSql, ps);
        }

        static string UpdateSql = "UPDATE [Orders] SET [Osn] = @Osn ,[AddTime] = @AddTime ,[PayAmount] = @PayAmount WHERE [OrderID]=@OrderID";
        public static void Update(Orders order)
        {
            SqlParameter[] ps = {
                                    AddInParam("@AddTime" , SqlDbType.VarChar, 50 , order.AddTime ) ,
                                    AddInParam("@OrderID" , SqlDbType.VarChar, 50 , order.OrderID  ) ,
                                    AddInParam("@Osn" , SqlDbType.VarChar, 50  , order.Osn ) ,
                                    AddInParam("@PayAmount" ,  SqlDbType.VarChar, 50 , order.PayAmount )
                                    };
            AccessDB.Execute(UpdateSql, ps);
        }
        static string DeleteOneSql = "Delete FROM [Orders] WHERE [OrderID]=@OrderID";
        public static void DeleteOne(int id)
        {
            SqlParameter[] ps = {
                                    AddInParam("@OrderID" , SqlDbType.VarChar, 50 , id )
                                    };
            AccessDB.Execute(DeleteOneSql, ps);
        }
        static string DeleteAllSql = "Delete FROM [Orders] ";
        public static void DeleteAll()
        {
            AccessDB.Execute(DeleteAllSql, null);
        }
        static string GetOneSql = "SELECT * FROM [Orders] WHERE [OrderID]=@OrderID";
        public static Orders GetOne(int id)
        {
            Orders order = new Model.Orders();
            SqlParameter[] ps = new SqlParameter[] {
                    new SqlParameter("@OrderID" , id )
                };
            AccessDB.SelectReader(GetOneSql, ps, delegate (IDataReader reader)
            {
                if (reader.Read())
                {
                    order.ToEntity(reader);
                }
            });
            return order;
        }
        public static Orders GetOneCache(string id)
        {
            Orders order = CacheProvider.Cache.Get(id, typeof(Orders)) as Orders;
            if (order != null)
                return order;
            SqlParameter[] ps = new SqlParameter[] {
                    new SqlParameter("@OrderID" , id )
                };

            AccessDB.SelectReader(GetOneSql, ps, delegate (IDataReader reader)
            {
                if (reader.Read())
                {
                    order = new Orders();
                    order.ToEntity(reader);
                    CacheProvider.Cache.Set(order.OrderID.ToString(), order, typeof(Orders));
                }
            });
            return order;
        }


        public static List<Orders> GetALLCache()
        {
            List<Orders> list = new List<Orders>();
            Orders o = new Orders();
            string id;
            AccessDB.SelectReader(GetAllSql, null, delegate (IDataReader reader)
            {
                while (reader.Read())
                {
                    id = reader["orderid"].ToString();
                    list.Add(GetOneCache(id));
                }
            });
            return list;
        }


        static string GetAllSql = "SELECT * FROM [Orders]";
        public static List<Orders> GetALL()
        {
            List<Orders> list = new List<Orders>();
            Orders o = new Orders();

            AccessDB.SelectReader(GetAllSql, null, delegate (IDataReader reader)
            {
                while (reader.Read())
                {
                    o = new Orders();
                    o.ToEntity(reader);
                    list.Add(o);
                }
            });
            return list;
        }
        static string PageSql = "SELECT TOP (@PageSize) OrderID, Osn, AddTime, PayAmount FROM (SELECT ROW_NUMBER() over(order by OrderID Desc) rowid, * FROM[Orders]) t WHERE rowid >(@PageSize*(@PageNumber-1)) ";
        public static List<Orders> Page(int pageSize, int pageNumber)
        {
            List<Orders> list = new List<Orders>();
            Orders o = new Orders();
            SqlParameter[] ps = new SqlParameter[] {
                    new SqlParameter("@pageSize" , pageSize ),
                    new SqlParameter("@pageNumber" , pageNumber )
                };
            AccessDB.SelectReader(PageSql, ps, delegate (IDataReader reader)
            {
                while (reader.Read())
                {
                    o = new Orders();
                    o.ToEntity(reader);
                    list.Add(o);
                }
            });
            return list;
        }
        static string TruncateSql = "Truncate TABLE [Orders];truncate table orderproducts";

        public static void Truncate()
        {
            AccessDB.Execute(TruncateSql, null);
        }

        static readonly string CountSql = "SELECT count(*) as count FROM Orders ";
        /// <summary> 获取库存同步白名单</summary>
        public static int Count()
        {
            int count = 0;
            AccessDB.SelectReader(CountSql, null, delegate (IDataReader reader)
           {
               if (reader.Read())
               {
                   count = Convert.ToInt32(reader["count"]);

               }
           });
            return count;
        }

    }
}

