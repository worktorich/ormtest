﻿using ORMTest.Core;
using ORMTest.Lin.Model.Model;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ORMTest.Lin
{
    public class TestClass : IORM
    {
        public void TestInsert(int count)
        {
            for (int i = 0; i < count; i++)
            {
                Orders o = new Orders { AddTime = DateTime.Now, OrderID = i, Osn = i + "s", PayAmount = i * 10 };
                OrderProducts op1 = new OrderProducts { OrderProductID = i, OrderID = o.OrderID, CateID = i, ProductName = "test" + i };
                OrderProducts op2 = new OrderProducts { OrderProductID = i + count, OrderID = o.OrderID, CateID = i + count, ProductName = "test" + i + count };
                Orders.Insert(o);
                OrderProducts.Insert(op1);
                OrderProducts.Insert(op2);
            }
        }

        public List<object> GetList()
        {
            return Orders.GetALL().ToList<object>();
        }

        public List<object> Page(int pageSize, int pageNumber)
        {
            return Orders.Page(pageSize, pageNumber).ToList<object>();

        }

        public object GetInfo(int id)
        {
            dynamic dy = new ExpandoObject();
            dy.Order = Orders.GetOne(id);
            dy.OrderProducts = OrderProducts.GetList(id);
            return dy;
        }

        public int Count()
        {
            return Orders.Count();
        }

        public object GetOne(int id)
        {
            return Orders.GetOne(id);
        }

        public void DeleteOne(int id)
        {
            Orders.DeleteOne(id);
        }

        public void DeleteAll()
        {
            Orders.DeleteAll();

        }


        public void UpdateOne<T>(T t)
        {
            Orders o = t as Orders;
            Orders.Update(o);
        }

        public void TruncateTable()
        {
            Orders.Truncate();
        }

        public static List<Orders> GetALLCache()
        {
            return Orders.GetALLCache();
        }

    }
}
