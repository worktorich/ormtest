﻿using DefaultConnect;
using ORMTest.Core;
using PetaPoco;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ORMTest.Petapoco
{
    public class TestClass : IORM
    {
        public static Database db = new Database("ConnectionStrings");

        public void TestInsert(int count)
        {

            for (int i = 0; i < count; i++)
            {
                Order o = new Order { AddTime = DateTime.Now, OrderID = i, Osn = i + "s", PayAmount = i * 10 };
                OrderProduct op1 = new OrderProduct { OrderProductID = i, OrderID = o.OrderID, CateID = i, ProductName = "test" + i };
                OrderProduct op2 = new OrderProduct { OrderProductID = i + count, OrderID = o.OrderID, CateID = i + count, ProductName = "test" + i + count };
                //DefaultConnectDB.GetInstance().Insert(o);
                //db.Insert(o);
                //Order.repo.Insert(o);
                db.Insert(o);
                db.Insert(op1);
                db.Insert(op2);
            }

        }

        public List<object> GetList()
        {
            Sql sql = new Sql();

            return db.Query<Order>(sql).ToList<object>();

        }

        public List<object> Page(int pageSize, int pageNumber)
        {
            return db.Page<Order>(pageSize, pageSize, new Sql()).Items.ToList<object>();

        }

        public object GetInfo(int id)
        {
            //dynamic dynamicObject = new ExpandoObject();
            //dynamicObject.Order =db.SingleOrDefault<Order>(id);
            //var sql = new Sql();
            //sql.Append("WHERE OrderID=@0", id);
            //dynamicObject.OrderProductList = db.Fetch<OrderProduct>(sql);
            //var sql = new Sql();
            //sql.Append("select * from orders where orderid=@0;", id);
            //sql.Append("select * from orderproducts where orderid=@0;", id);
            dynamic dy = new ExpandoObject();
            dy.Order = db.SingleOrDefault<Order>(id);
            var sql = new Sql();
            sql.Append("WHERE orderid=@0", id);
            dy.OrderProductList = db.Query<OrderProduct>(sql);
            return dy;

        }

        public int Count()
        {
            var sql = new Sql();
            sql.Append("select count(*) from orders");
            return db.ExecuteScalar<int>(sql);
        }



        public object GetOne(int id)
        {
            return db.SingleOrDefault<Order>(id);
        }


        public void DeleteOne(int id)
        {
            db.Delete<Order>(id);
        }

        public void DeleteAll()
        {

            db.Execute("delete Orders ");
        }


        public void UpdateOne<T>(T t)
        {
            db.Update(t);
        }

        public void TruncateTable()
        {
            db.Execute("truncate table Orders");
            db.Execute("truncate table orderproducts");
        }


    }
}
