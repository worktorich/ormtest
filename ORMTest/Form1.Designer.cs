﻿namespace ORMTest
{
    partial class Form1
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tbCount = new System.Windows.Forms.TextBox();
            this.tbXc = new System.Windows.Forms.TextBox();
            this.btnTest = new System.Windows.Forms.Button();
            this.cbEF = new System.Windows.Forms.CheckBox();
            this.cbPP = new System.Windows.Forms.CheckBox();
            this.cbEL = new System.Windows.Forms.CheckBox();
            this.cbLin = new System.Windows.Forms.CheckBox();
            this.listView1 = new System.Windows.Forms.ListView();
            this.btnLinCacheTest = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(43, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "测试数量";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(232, 26);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 12);
            this.label2.TabIndex = 1;
            this.label2.Text = "线程数量";
            // 
            // tbCount
            // 
            this.tbCount.Location = new System.Drawing.Point(102, 17);
            this.tbCount.Name = "tbCount";
            this.tbCount.Size = new System.Drawing.Size(105, 21);
            this.tbCount.TabIndex = 2;
            this.tbCount.Text = "1000";
            // 
            // tbXc
            // 
            this.tbXc.Enabled = false;
            this.tbXc.Location = new System.Drawing.Point(307, 17);
            this.tbXc.Name = "tbXc";
            this.tbXc.Size = new System.Drawing.Size(105, 21);
            this.tbXc.TabIndex = 3;
            this.tbXc.Text = "1";
            // 
            // btnTest
            // 
            this.btnTest.Location = new System.Drawing.Point(506, 15);
            this.btnTest.Name = "btnTest";
            this.btnTest.Size = new System.Drawing.Size(75, 23);
            this.btnTest.TabIndex = 4;
            this.btnTest.Text = "开始测试";
            this.btnTest.UseVisualStyleBackColor = true;
            this.btnTest.Click += new System.EventHandler(this.btnTest_Click);
            // 
            // cbEF
            // 
            this.cbEF.AutoSize = true;
            this.cbEF.Checked = true;
            this.cbEF.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbEF.Location = new System.Drawing.Point(45, 67);
            this.cbEF.Name = "cbEF";
            this.cbEF.Size = new System.Drawing.Size(36, 16);
            this.cbEF.TabIndex = 5;
            this.cbEF.Text = "EF";
            this.cbEF.UseVisualStyleBackColor = true;
            // 
            // cbPP
            // 
            this.cbPP.AutoSize = true;
            this.cbPP.Checked = true;
            this.cbPP.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbPP.Location = new System.Drawing.Point(160, 67);
            this.cbPP.Name = "cbPP";
            this.cbPP.Size = new System.Drawing.Size(72, 16);
            this.cbPP.TabIndex = 6;
            this.cbPP.Text = "Petapoco";
            this.cbPP.UseVisualStyleBackColor = true;
            // 
            // cbEL
            // 
            this.cbEL.AutoSize = true;
            this.cbEL.Checked = true;
            this.cbEL.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbEL.Location = new System.Drawing.Point(289, 67);
            this.cbEL.Name = "cbEL";
            this.cbEL.Size = new System.Drawing.Size(60, 16);
            this.cbEL.TabIndex = 7;
            this.cbEL.Text = "Eleven";
            this.cbEL.UseVisualStyleBackColor = true;
            // 
            // cbLin
            // 
            this.cbLin.AutoSize = true;
            this.cbLin.Checked = true;
            this.cbLin.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbLin.Location = new System.Drawing.Point(404, 67);
            this.cbLin.Name = "cbLin";
            this.cbLin.Size = new System.Drawing.Size(42, 16);
            this.cbLin.TabIndex = 8;
            this.cbLin.Text = "Lin";
            this.cbLin.UseVisualStyleBackColor = true;
            // 
            // listView1
            // 
            this.listView1.Location = new System.Drawing.Point(45, 107);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(791, 395);
            this.listView1.TabIndex = 9;
            this.listView1.UseCompatibleStateImageBehavior = false;
            // 
            // btnLinCacheTest
            // 
            this.btnLinCacheTest.Location = new System.Drawing.Point(506, 60);
            this.btnLinCacheTest.Name = "btnLinCacheTest";
            this.btnLinCacheTest.Size = new System.Drawing.Size(75, 23);
            this.btnLinCacheTest.TabIndex = 10;
            this.btnLinCacheTest.Text = "Lin缓存读取测试";
            this.btnLinCacheTest.UseVisualStyleBackColor = true;
            this.btnLinCacheTest.Click += new System.EventHandler(this.btnLinCacheTest_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(881, 563);
            this.Controls.Add(this.btnLinCacheTest);
            this.Controls.Add(this.listView1);
            this.Controls.Add(this.cbLin);
            this.Controls.Add(this.cbEL);
            this.Controls.Add(this.cbPP);
            this.Controls.Add(this.cbEF);
            this.Controls.Add(this.btnTest);
            this.Controls.Add(this.tbXc);
            this.Controls.Add(this.tbCount);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbCount;
        private System.Windows.Forms.TextBox tbXc;
        private System.Windows.Forms.Button btnTest;
        private System.Windows.Forms.CheckBox cbEF;
        private System.Windows.Forms.CheckBox cbPP;
        private System.Windows.Forms.CheckBox cbEL;
        private System.Windows.Forms.CheckBox cbLin;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.Button btnLinCacheTest;
    }
}

