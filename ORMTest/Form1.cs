﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ORMTest.EF;
using ORMTest.Eleven;
using ORMTest.Lin;
using ORMTest.Petapoco;
using ORMTest.Core;
using log4net;
using System.Reflection;
using System.Dynamic;

[assembly: log4net.Config.XmlConfigurator(Watch = true)]
namespace ORMTest
{
    public partial class Form1 : Form
    {
        ILog log = log4net.LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        IORM orm;
        public Form1()
        {
            InitializeComponent();
        }
        int count = 0;
        private void btnTest_Click(object sender, EventArgs e)
        {
            count = int.Parse(tbCount.Text);
            Stopwatch sw = new Stopwatch();
            sw.Start();
            if (cbEF.Checked)
            {
                log.Info("===========框架EF开始===========");
                orm = new EF.TestClass();
                Test(orm);
                log.Info("===========框架EF结束===========");
            }
            if (cbPP.Checked)
            {
                log.Info("===========框架petapoco开始===========");
                orm = new Petapoco.TestClass();
                Test(orm);
                log.Info("===========框架petapoco结束===========");
            }

            if (cbEL.Checked)
            {
                log.Info("===========框架eleven开始===========");
                orm = new Eleven.TestClass();
                Test(orm);
                log.Info("===========框架eleven结束===========");
            }

            if (cbLin.Checked)
            {
                log.Info("框架Lin开始");
                orm = new Lin.TestClass();
                Test(orm);
                log.Info("框架Lin结束");
            }
            MessageBox.Show("测试完毕");
        }

        void TestCache()
        {
            orm = new Lin.TestClass();
            orm.TestInsert(int.Parse(tbCount.Text));
            sw.Restart();
            var list = Lin.Model.Model.Orders.GetALLCache();
            sw.Stop();
            Log("GetAllCache", sw.ElapsedMilliseconds);
            MessageBox.Show(sw.ElapsedMilliseconds.ToString());
        }

        Stopwatch sw = new Stopwatch();
        static string msglog = "{0}:耗时{1}毫秒，{2}秒";
        void Test(IORM orm)
        {
            //try
            //{
            int count = int.Parse(tbCount.Text);

            sw.Restart();
            orm.TestInsert(count);
            sw.Stop();
            Log(MethodEnum.Insert, sw.ElapsedMilliseconds);
            sw.Restart();
            for (int i = 0; i < count; i++)
            {
                orm.GetList();
            }
            sw.Stop();
            Log(MethodEnum.GetList, sw.ElapsedMilliseconds);


            sw.Restart();
            for (int i = 0; i < count; i++)
            {
                orm.Page(100, 1);
            }
            sw.Stop();
            Log(MethodEnum.Page, sw.ElapsedMilliseconds);
            sw.Restart();
            for (int i = 0; i < count; i++)
            {
                var t1 = orm.GetInfo(i);
            }
            sw.Stop();
            Log(MethodEnum.GetInfo, sw.ElapsedMilliseconds);
            sw.Restart();
            dynamic o = new ExpandoObject();
            for (int i = 0; i < count; i++)
            {
                o = orm.GetOne(i);
            }
            sw.Stop();
            Log(MethodEnum.GetOne, sw.ElapsedMilliseconds);
            sw.Restart();
            for (int i = 0; i < count; i++)
            {
                orm.UpdateOne(o);
            }
            sw.Stop();

            Log(MethodEnum.UpdateOne, sw.ElapsedMilliseconds);
            sw.Restart();
            for (int i = 0; i < count; i++)
            {
                orm.Count();
            }
            sw.Stop();

            Log(MethodEnum.Count, sw.ElapsedMilliseconds);
            sw.Restart();
            for (int i = 0; i < count; i++)
            {
                orm.DeleteOne(i);
            }
            sw.Stop();

            Log(MethodEnum.DeleteOne, sw.ElapsedMilliseconds);
            sw.Restart();
            for (int i = 0; i < count; i++)
            {
                orm.DeleteAll();
            }
            sw.Stop();

            Log(MethodEnum.DeleteAll, sw.ElapsedMilliseconds);
            sw.Restart();
            orm.TruncateTable();
            sw.Stop();
            Log(MethodEnum.TruncateTable, sw.ElapsedMilliseconds);
            //}
            //catch (Exception e)
            //{
            //    log.Error("运行错误" + orm.GetType().FullName, e);
            //}
        }

        void Log(string method, long elapsedMilliseconds)
        {
            log.Info(string.Format(msglog, method, elapsedMilliseconds, elapsedMilliseconds / 1000));
        }

        private void btnLinCacheTest_Click(object sender, EventArgs e)
        {
            TestCache();
        }
    }


    public class MethodEnum
    {
        public static string Insert = "Insert";
        public static string GetList = "GetList";
        public static string Page = "Page";
        public static string GetInfo = "GetInfo";
        public static string GetOne = "GetOne";
        public static string Count = "Count";
        public static string DeleteOne = "DeleteOne";
        public static string DeleteAll = "DeleteAll";
        public static string UpdateOne = "UpdateOne";
        public static string TruncateTable = "TruncateTable";
    }
}
